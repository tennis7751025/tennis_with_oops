class TennisGame:
    def __init__(self):
        self.score = {'A': {'sets': 0, 'games': 0, 'points': 0}, 'B': {'sets': 0, 'games': 0, 'points': 0}}

    def calculate_points(self, string):
        for char in string:
            if self.score['A']['sets'] == 2 or self.score['B']['sets'] == 2:
                break

            player, opponent = ('A', 'B') if char == 'A' else ('B', 'A')
            if self.score[player]['points'] == 4 or self.score[opponent]['points'] == 4:
                self.score[player]['points'], self.score[opponent]['points'], self.score[player]['games'], self.score[opponent]['games'] = self.play_advantage(char, self.score[player]['points'], self.score[opponent]['points'], self.score[player]['games'], self.score[opponent]['games'])
            elif self.score[player]['points'] == 3 and self.score[opponent]['points'] < 3:
                self.score[player]['games'] += 1
                self.score[player]['points'] = self.score[opponent]['points'] = 0
                self.score[player]['sets'], self.score[opponent]['sets'], self.score[player]['games'], self.score[opponent]['games'] = self.check_set_winner(self.score[player]['games'], self.score[opponent]['games'], self.score[player]['sets'], self.score[opponent]['sets'])
            elif self.score[player]['points'] == 3 and self.score[opponent]['points'] == 3:
                self.score[player]['points'] = 4
            else:
                self.score[player]['points'] += 1

    def play_advantage(self, char, A_points, B_points, A_games, B_games):
        player, opponent = ('A', 'B') if char == 'A' else ('B', 'A')
        if A_points == 4:
            A_games += 1
            A_points = B_points = 0
        elif B_points == 4:
            B_points = 3
        else:
            A_points += 1
        return A_points, B_points, A_games, B_games

    def check_set_winner(self, player_games, opponent_games, player_sets, opponent_sets):
        if player_games >= 6 and (player_games - opponent_games) >= 2:
            player_sets += 1
            player_games = opponent_games = 0
        elif opponent_games >= 6 and (opponent_games - player_games) >= 2:
            opponent_sets += 1
            player_games = opponent_games = 0
        return player_sets, opponent_sets, player_games, opponent_games

    def print_scoreboard(self, string):
        self.calculate_points(string)
        points = {0: 0, 1: 15, 2: 30, 3: 40, 4: "AD"}
        print("--------------------------------------------")
        print("| Player  |   Sets   |   Games   |  Points |")
        print("--------------------------------------------")
        print(f"|Player A |    {self.score['A']['sets']}     |     {self.score['A']['games']}     |    {points[self.score['A']['points']]}   |")
        print(f"|Player B |    {self.score['B']['sets']}     |     {self.score['B']['games']}     |    {points[self.score['B']['points']]}   |")
        print("--------------------------------------------")
        if self.score['A']['sets'] == 2:
            print("Player A wins the match!")
        elif self.score['B']['sets'] == 2:
            print("Player B wins the match!")

string = "BAABBAABAAABBBABABABABABBBABBAAAAABBBBBABABBABABABABABAABBBBBABABABABBABABBABABBBBBB"
game = TennisGame()
game.print_scoreboard(string)