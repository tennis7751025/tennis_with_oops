def calculate_points(string):
    A_points = B_points = A_games = B_games = A_sets = B_sets = 0

    for char in string:
        if A_sets == 2 or B_sets == 2:  
            break

        if A_points == 4 or B_points == 4:
            A_points, B_points, A_games, B_games = play_advantage(char, A_points, B_points, A_games, B_games)
        elif char == 'A':
            if A_points == 3 and B_points < 3:
                A_games += 1
                A_points = B_points = 0
                A_sets, B_sets, A_games, B_games = check_set_winner(A_games, B_games, A_sets, B_sets)
            elif A_points == 3 and B_points == 3:
                A_points = 4
            else:
                A_points += 1
        elif char == 'B':
            if B_points == 3 and A_points < 3:
                B_games += 1
                A_points = B_points = 0
                A_sets, B_sets, A_games, B_games = check_set_winner(B_games, A_games, B_sets, A_sets)
            elif B_points == 3 and A_points == 3:
                B_points = 4
            else:
                B_points += 1

    return A_points, B_points, A_games, B_games, A_sets, B_sets

def play_advantage(char, A_points, B_points, A_games, B_games):
    if char == 'A':
        if A_points == 4:
            A_games += 1
            A_points = B_points = 0
        elif B_points == 4:
            B_points = 3
        else:
            A_points += 1
    elif char == 'B':
        if B_points == 4:
            B_games += 1
            A_points = B_points = 0
        elif A_points == 4:
            A_points = 3
        else:
            B_points += 1

    return A_points, B_points, A_games, B_games

def check_set_winner(player_games, opponent_games, player_sets, opponent_sets):
    if player_games >= 6 and (player_games - opponent_games) >= 2:
        player_sets += 1
        player_games = opponent_games = 0
    elif opponent_games >= 6 and (opponent_games - player_games) >= 2:
        opponent_sets += 1
        player_games = opponent_games = 0
    return player_sets, opponent_sets, player_games, opponent_games


def print_scoreboard(string):
    A_points, B_points, A_games, B_games, A_sets, B_sets = calculate_points(string)
    points = {0: 0, 1: 15, 2: 30, 3: 40, 4: "AD"}
    print("--------------------------------------------")
    print("| Player  |   Sets   |   Games   |  Points |")
    print("--------------------------------------------")
    print(f"|Player A |    {A_sets}     |     {A_games}     |    {points[A_points]}   |")
    print(f"|Player B |    {B_sets}     |     {B_games}     |    {points[B_points]}   |")
    print("--------------------------------------------")
    if A_sets == 2:
        print("Player A wins the match!")
    elif B_sets == 2:
        print("Player B wins the match!")

string = "BAABBABBAABAABBBBBBBBAAABBBBAAAABABABABABABABABABAAAAAAABBBBBBBBABABABABABBABABBBBBBAAAAAAAABBBBAAAAABAABABAAAABBAAAAAAABBBBBBBBBBBBBBAAABBBBAAAABABABABABABABABAAAAAABBBBABAAAAAAA"
print_scoreboard(string)
