class TennisGame:
    def __init__(self):
        self.points = {'A': 0, 'B': 0}
        self.games = {'A': 0, 'B': 0}
        self.sets = {'A': 0, 'B': 0}

    def calculate_points(self, string):
        for char in string:
            if self.sets['A'] == 2 or self.sets['B'] == 2:
                break

            if self.points['A'] == 4 or self.points['B'] == 4:
                self.play_advantage(char)
            else:
                self.win_point(char)

    def play_advantage(self, char):
        opponent = 'A' if char == 'B' else 'B'
        if self.points[char] == 4:
            self.win_game(char)
        elif self.points[opponent] == 4:
            self.points[opponent] = 3
        else:
            self.points[char] += 1

    def win_point(self, char):
        opponent = 'A' if char == 'B' else 'B'
        if self.points[char] == 3 and self.points[opponent] < 3:
            self.win_game(char)
        elif self.points[char] == 3 and self.points[opponent] == 3:
            self.points[char] = 4
        else:
            self.points[char] += 1

    def win_game(self, char):
        opponent = 'A' if char == 'B' else 'B'
        self.games[char] += 1
        self.points['A'] = self.points['B'] = 0
        self.check_set_winner(char, opponent)

    def check_set_winner(self, char, opponent):
        if self.games[char] >= 6 and self.games[char] - self.games[opponent] >= 2:
            self.sets[char] += 1
            self.games['A'] = self.games['B'] = 0

    def print_scoreboard(self, string):
        self.calculate_points(string)
        points = {0: 0, 1: 15, 2: 30, 3: 40, 4: "AD"}
        print("--------------------------------------------")
        print("| Player  |   Sets   |   Games   |  Points |")
        print("--------------------------------------------")
        print(f"|Player A |    {self.sets['A']}     |     {self.games['A']}     |    {points[self.points['A']]}   |")
        print(f"|Player B |    {self.sets['B']}     |     {self.games['B']}     |    {points[self.points['B']]}   |")
        print("--------------------------------------------")
        if self.sets['A'] == 2:
            print("Player A wins the match!")
        elif self.sets['B'] == 2:
            print("Player B wins the match!")

string = "BAABBAABAAAAAABABABABBAABBABBBABABABABABABABABAAABBBBABABBABAABBBBBBAAAABBABABABABABBABABABABABBABAABABAAAAAAABBBBABABBB"
game = TennisGame()
game.print_scoreboard(string)
